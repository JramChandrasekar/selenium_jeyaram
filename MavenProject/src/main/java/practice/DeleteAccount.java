package practice;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DeleteAccount {

	public static void main(String[] args) throws InterruptedException {
		//System.setProperty("Webdriver.Chrome.driver","C://chromedriver.exe");
				WebDriverManager.chromedriver().setup();
				ChromeOptions opt = new ChromeOptions();
		        opt.addArguments("--disable-notifications");
		        
		        // Webdrivermanager downloads the suitable chrome driver that matches with the browser 
		        WebDriverManager.chromedriver().setup();
		        
		        //Launching Chrome browser
		        ChromeDriver driver = new ChromeDriver(opt);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.get("https://login.salesforce.com");
				driver.manage().window().maximize();
				driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
				driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
				driver.findElement(By.id("Login")).click();
				Thread.sleep(9000);
				String Name = "Jeyaram_Edit";
				driver.findElement(By.className("slds-icon-waffle")).click();
				Thread.sleep(9000);
				driver.findElement(By.xpath("//button[text()='View All']")).click();
				Thread.sleep(9000);
				driver.findElement(By.xpath("//input[@class = 'slds-input']")).sendKeys("accounts");
				Thread.sleep(9000);
				driver.findElement(By.xpath("//mark[text()='Accounts']")).click();
				Thread.sleep(9000);
				driver.findElement(By.xpath("//input[@name='Account-search-input']")).sendKeys(Name);
				Thread.sleep(9000);
				driver.findElement(By.xpath("//input[@name='Account-search-input']")).sendKeys(Keys.ENTER);
				Thread.sleep(9000);
				driver.findElement(By.xpath("(//a[@title='Jeyaram_Edit']/following::a[@role='button'])[1]")).click();
				driver.findElement(By.xpath("//a[@title='Delete']")).click();
				driver.findElement(By.xpath("//span[text()='Delete']")).click();

	}

}
