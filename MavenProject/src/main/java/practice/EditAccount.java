package practice;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class EditAccount {
	public static void main(String[] args) throws InterruptedException {
		//System.setProperty("Webdriver.Chrome.driver","C://chromedriver.exe");
		WebDriverManager.chromedriver().setup();
		ChromeOptions opt = new ChromeOptions();
        opt.addArguments("--disable-notifications");
        
        // Webdrivermanager downloads the suitable chrome driver that matches with the browser 
        WebDriverManager.chromedriver().setup();
        
        //Launching Chrome browser
        ChromeDriver driver = new ChromeDriver(opt);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		Thread.sleep(9000);
		String Name = "JramChandrasekar";
		String Name1 = "Jeyaram_Edit";
		driver.findElement(By.className("slds-icon-waffle")).click();
		Thread.sleep(9000);
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		Thread.sleep(9000);
		driver.findElement(By.xpath("//input[@class = 'slds-input']")).sendKeys("accounts");
		Thread.sleep(9000);
		driver.findElement(By.xpath("//mark[text()='Accounts']")).click();
		Thread.sleep(9000);
		driver.findElement(By.xpath("//input[@name='Account-search-input']")).sendKeys(Name);
		Thread.sleep(9000);
		driver.findElement(By.xpath("//input[@name='Account-search-input']")).sendKeys(Keys.ENTER);
		Thread.sleep(9000);
		driver.findElement(By.xpath("(//a[@title='"+Name+"']/following::a[@role='button'])[1]")).click();
		driver.findElement(By.xpath("//a[@title='Edit']")).click();
		Thread.sleep(9000);
		driver.findElement(By.xpath("//input[@name='Name']")).clear();
		driver.findElement(By.xpath("//input[@name='Name']")).sendKeys(Name1);
		Thread.sleep(9000);
		driver.findElement(By.xpath("//input[@name='Phone']")).sendKeys("9898989898");
		Thread.sleep(9000);
		driver.findElement(By.xpath("(//button[@class='slds-combobox__input slds-input_faux slds-combobox__input-value'])[2]")).click();
		driver.findElement(By.xpath("//lightning-base-combobox-item[@data-value='Technology Partner']")).click();
		Thread.sleep(9000);
		WebElement element = driver.findElement(By.xpath("(//button[@class='slds-combobox__input slds-input_faux slds-combobox__input-value'])[4]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Thread.sleep(9000);
		WebElement element1 = driver.findElement(By.xpath("//lightning-base-combobox-item[@data-value='Healthcare']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		driver.findElement(By.xpath("(//textarea[@autocomplete='street-address'])[1]")).sendKeys("Test billing");
		Thread.sleep(9000);
		driver.findElement(By.xpath("(//textarea[@autocomplete='street-address'])[2]")).sendKeys("Test shipping");
		Thread.sleep(9000);
		driver.findElement(By.xpath("(//input[@autocomplete='address-level2'])[1]")).sendKeys("Test Billing city");
		Thread.sleep(9000);
		driver.findElement(By.xpath("(//input[@autocomplete='address-level2'])[2]")).sendKeys("Test shipping city");
		Thread.sleep(9000);
		WebElement element2 = driver.findElement(By.xpath("(//button[@class='slds-combobox__input slds-input_faux slds-combobox__input-value'])[5]"));
		JavascriptExecutor executor2 = (JavascriptExecutor)driver;
		executor2.executeScript("arguments[0].click();", element2);
		WebElement element3 = driver.findElement(By.xpath("//lightning-base-combobox-item[@data-value='Low']"));
		JavascriptExecutor executor3 = (JavascriptExecutor)driver;
		executor3.executeScript("arguments[0].click();", element3);
		Thread.sleep(9000);
		WebElement element4 = driver.findElement(By.xpath("(//button[@class='slds-combobox__input slds-input_faux slds-combobox__input-value'])[6]"));
		JavascriptExecutor executor4 = (JavascriptExecutor)driver;
		executor4.executeScript("arguments[0].click();", element4);
		WebElement element5 = driver.findElement(By.xpath("//lightning-base-combobox-item[@data-value='Silver']"));
		JavascriptExecutor executor5 = (JavascriptExecutor)driver;
		executor5.executeScript("arguments[0].click();", element5);
		Thread.sleep(9000);
		WebElement element6 = driver.findElement(By.xpath("(//button[@class='slds-combobox__input slds-input_faux slds-combobox__input-value'])[7]"));
		JavascriptExecutor executor6 = (JavascriptExecutor)driver;
		executor6.executeScript("arguments[0].click();", element6);
		WebElement element7 = driver.findElement(By.xpath("//lightning-base-combobox-item[@data-value='No']"));
		JavascriptExecutor executor7 = (JavascriptExecutor)driver;
		executor7.executeScript("arguments[0].click();", element7);
		Thread.sleep(9000);
		WebElement element8 = driver.findElement(By.xpath("(//button[@class='slds-combobox__input slds-input_faux slds-combobox__input-value'])[8]"));
		JavascriptExecutor executor8 = (JavascriptExecutor)driver;
		executor8.executeScript("arguments[0].click();", element8);
		driver.findElement(By.xpath("//button[text()='Save']")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[@class='slds-button slds-show']")).click();
		Thread.sleep(9000);
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		Thread.sleep(9000);
		driver.findElement(By.xpath("(//input[@class = 'slds-input'])[2]")).sendKeys("accounts");
		Thread.sleep(9000);
		driver.findElement(By.xpath("//mark[text()='Accounts']")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@name='Account-search-input']")).sendKeys(Name1);
		Thread.sleep(9000);
		driver.findElement(By.xpath("//input[@name='Account-search-input']")).sendKeys(Keys.ENTER);
		String output = driver.findElement(By.xpath("//a[@title='"+Name1+"']")).getText();
		if(output.contains(Name))
		{
			System.out.println("Account " +Name1+ " is available");
		}
		else {
			System.out.println("The account is not available");
		}
			
					
		}
	
		
	}


