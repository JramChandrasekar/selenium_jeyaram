package practice;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.functors.SwitchTransformer;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class FirstAssessment_Jeyaram{

	public static void main(String[] args) throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		ChromeOptions opt = new ChromeOptions();
        opt.addArguments("--disable-notifications");
        
        // Webdrivermanager downloads the suitable chrome driver that matches with the browser 
        WebDriverManager.chromedriver().setup();
        
        //Launching Chrome browser
        ChromeDriver driver = new ChromeDriver(opt);
		driver.manage().timeouts().implicitlyWait(Duration.ofMillis(5000));
		//Step1: Login to https://login.salesforce.com
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		Thread.sleep(3000);
		//Step2: Click on toggle menu button from the left corner
		String Name = "Jeyaram_Workout";
		driver.findElement(By.className("slds-icon-waffle")).click();
		Thread.sleep(9000);
		//Step3: Click View all
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		//Step4: Click Service Console from App Launcher 
		driver.findElement(By.xpath("//p[contains(text(),'Lets support agents')]")).click();
		Thread.sleep(9000);
		driver.findElement(By.xpath("//button[@title='Show Navigation Menu']")).click();
		Thread.sleep(9000);
		//Dropdown selection
		driver.findElement(By.xpath("//span[text()='Dashboards']")).click();
		Thread.sleep(9000);
		driver.findElement(By.xpath("//div[text()='New Dashboard']")).click();
		Thread.sleep(9000);
		//Dashboard creation
		WebElement frame = driver.findElement(By.xpath("//iframe[@title='dashboard']"));
		driver.switchTo().frame(frame);
		driver.findElement(By.id("dashboardNameInput")).sendKeys(Name);
		Thread.sleep(3000);
		driver.findElement(By.id("dashboardDescriptionInput")).sendKeys("Testing");
		Thread.sleep(9000);
		driver.findElement(By.xpath("//button[text()='Create']")).click();
		driver.switchTo().defaultContent();
		WebElement frame1 = driver.findElement(By.xpath("//iframe[@title='dashboard']"));
		driver.switchTo().frame(frame1);
		WebElement element = driver.findElement(By.xpath("//button[text()='Done']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Thread.sleep(5000);
		//Dashboard creation verification
		String title = driver.findElement(By.xpath("//span[@class='slds-page-header__title slds-truncate']")).getText();
		System.out.println(title);
		//Dashboard subscription
		driver.findElement(By.xpath("//button[text()='Subscribe']")).click();
		driver.switchTo().defaultContent();
		driver.findElement(By.xpath("//span[text()='Daily']")).click();
		Thread.sleep(5000);
		Select time = new Select (driver.findElement(By.id("time")));
		time.selectByValue("10");
		WebElement save = driver.findElement(By.xpath("//span[text()='Save']"));
		executor.executeScript("arguments[0].click()",save);		
		Thread.sleep(5000);
		//Subscription message verification
		String toastMessage = driver.findElement(By.xpath("//span[@class = 'toastMessage slds-text-heading--small forceActionsText']")).getText();
		System.out.println(toastMessage);
		driver.findElement(By.xpath("//button[@title= 'Close "+Name+"']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//a[@title='Dashboards']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//a[@title='Private Dashboards'])[1]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//input[@placeholder='Search private dashboards...']")).sendKeys(Name);
		Thread.sleep(5000);
		//Deletion of the dashboard
		@SuppressWarnings("deprecation")
		WebDriverWait wait = new WebDriverWait(driver, 6);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']/tbody/tr/td[6]")));
	    WebElement button = driver.findElement(By.xpath("//table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']/tbody/tr/td[6]"));
	    executor.executeScript("arguments[0].scrollIntoView();", button);
	    driver.findElement(By.xpath("((//span[text()='"+Name+"'])/following::button[@class='slds-button slds-button_icon-border slds-button_icon-x-small'])[1]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//span[text()='Delete']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//button[@title='Delete']")).click();
		Thread.sleep(5000);
		String deleted = driver.findElement(By.xpath("//span[@class = 'toastMessage slds-text-heading--small forceActionsText']")).getText();
		Thread.sleep(5000);
		System.out.println(deleted);
		driver.findElement(By.xpath("//input[@placeholder='Search private dashboards...']")).sendKeys(Name);
		Thread.sleep(5000);
		String result = driver.findElement(By.xpath("//span[@class='emptyMessageTitle']")).getText();
		Thread.sleep(5000);
		System.out.println(result);

	}

}
